from rest_framework import serializers
from snippets.models import Snippet, LANGUAJE_CHOICES, STYLE_CHOICES


#forma larga
#class SnippetSerializer(serializers.Serializer):
#	id = serializers.IntegerField(read_only=True)
#	title = serializers.CharField(required=False, allow_blank=True)
#	code = serializers.CharField(style={'base_template':'textarea.html'})
#	linenos = serializers.BooleanField(required=False)
#	languaje = serializers.ChoiceField(choices=LANGUAJE_CHOICES, default='python')
#	style = serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')

#	def create(self, validated_data):
#		return Snippet.objects.create(**validated_data)

#	def update(self, instance, validated_data):
#		"""update and return an existing snippet instance, given validated data"""
#		instance.title = validated_data.get('title', instance.title)
#		instance.code = validated_data.get('code', instance.code)
#		instance.linenos = validated_data.get('linenos', instance.linenos)
#		instance.languaje = validated_data.get('languaje', instance.languaje)
#		instance.style  = validated_data.get('style', instance.style)
#		instance.save()
#		return instance

#forma consisa con modelserializers
class SnippetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Snippet
		fields = ('id', 'title', 'code', 'linenos', 'languaje', 'style')